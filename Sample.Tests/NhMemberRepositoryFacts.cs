﻿using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using Sample.Core;
using Sample.Data;
using Sample.Data.Maps;
using Xunit;

namespace Sample.Tests
{
    public class NhMemberRepositoryFacts : IDisposable
    {
        private readonly ISession session;
        private readonly IMemberRepository repository;

        public NhMemberRepositoryFacts()
        {
            session = Fluently.Configure()
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MemberMap>())
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromAppSetting("test")))
                    .BuildSessionFactory().OpenSession();
            
            repository = new NhMemberRepository(session);
        }

        [Fact]
        public void ItCanGetMemberFromDatabase()
        {
            var member = repository.GetMemberByUsername("me@cprieto.com");
            Assert.NotNull(member);
            Assert.Equal("me@cprieto.com", member.Email);
        }

        [Fact]
        public void ItCanGetSiteFromMember()
        {
            var member = repository.GetMemberByUsername("me@cprieto.com");
            Assert.NotNull(member.Site);
        }

        public void Dispose()
        {
            session.Close();
        }
    }
}