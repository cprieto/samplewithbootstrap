﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Sample.Tests
{
    public class DefaultTemplateRendererFacts
    {
        private readonly ITemplateRenderer renderer;

        public DefaultTemplateRendererFacts()
        {
            renderer = new DefaultTemplateRenderer();
        }

        [Fact]
        public void ItCanGetPlaceholderNamesFromTemplate()
        {
            const string template = "this is your name: $name$ and you are $age$ years old";
            var placeholders = renderer.GetPlaceholders(template);
            
            Assert.Equal(2, placeholders.Count());
            Assert.True(placeholders.Contains("$name$"));
            Assert.True(placeholders.Contains("$age$"));
        }

        [Fact]
        public void ItCanReplacePlaceholdersWithValues()
        {
            const string template = "this is your name: $name$ and you are $age$ years old";
            const string expected = "this is your name: Cristian and you are 30 years old";

            var dictionary = new Dictionary<string, string>
                {
                    {"$name$", "Cristian"},
                    {"$age$", "30"}
                };

            var result = renderer.Render(template, dictionary);

            Assert.Equal(expected, result);
        }
    }
}