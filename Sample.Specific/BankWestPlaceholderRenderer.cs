﻿using System.Collections.Generic;
using NHibernate;

namespace Sample.Specific
{
    [PlaceholderRendererMetadata("$money$", 2)]
    public class BankWestPlaceholderRenderer : IPlaceholderRenderer
    {
        public void Populate(IDictionary<string, string> dataBag)
        {
            dataBag["$money$"] = "1000";
        }
    }
}