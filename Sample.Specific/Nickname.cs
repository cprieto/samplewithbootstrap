﻿using FluentNHibernate.Mapping;
using NHibernate;

namespace Sample.Specific
{
    public class Nickname
    {
        public virtual string Username { get; protected set; }
        public virtual string Nick { get; protected set; }
    }

    public class NicknameMap : ClassMap<Nickname>
    {
        public NicknameMap()
        {
            Id(x => x.Username)
                .GeneratedBy.Assigned()
                .Column("username");
            Map(x => x.Nick)
                .Column("Nickname")
                .ReadOnly();
        }
    }

    public interface INicknameRepository
    {
        Nickname GetNicknameForUser(string username);
    }

    class NicknameRepository : INicknameRepository
    {
        private readonly ISession session;

        public NicknameRepository(ISession session)
        {
            this.session = session;
        }

        public Nickname GetNicknameForUser(string username)
        {
            return session.Get<Nickname>(username);
        }
    }
}