﻿using System.Linq;
using Autofac;

namespace Sample.Specific
{
    public class BankWestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var types = ThisAssembly.GetTypes()
                        .Where(t => t.GetInterface("IPlaceholderRenderer") != null);

            foreach (var type in types)
            {
                var attribute = type.GetCustomAttributes(typeof (PlaceholderRendererMetadataAttribute), true).OfType<PlaceholderRendererMetadataAttribute>().First();

                builder.RegisterType(type)
                    .As<IPlaceholderRenderer>()
                    .WithMetadata<PlaceholderRendererMetadata>(c =>
                        {
                            c.For(m => m.Name, attribute.Name);
                            c.For(m => m.SiteId, attribute.SiteId);
                        });
            }
        }
    }
}