﻿using FluentNHibernate.Mapping;

namespace Sample.Data.Maps
{
    public class SiteMap : ClassMap<Site>
    {
        public SiteMap()
        {
            Id(x => x.Id)
                .Column("Id")
                .GeneratedBy.Identity();
            Map(x => x.Name)
                .Column("Name")
                .Not.Nullable();
        }
    }
}