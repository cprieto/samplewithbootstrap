﻿using FluentNHibernate.Mapping;

namespace Sample.Data.Maps
{
    public class TemplateMap : ClassMap<Template>
    {
        public TemplateMap()
        {
            Table("Template");
            Id(x => x.Id)
                .Column("Id")
                .GeneratedBy.Identity();
            Map(x => x.Name)
                .Column("Name")
                .ReadOnly();
            Map(x => x.Content)
                .Column("Text")
                .ReadOnly();
            References(x => x.Site)
                .Column("SiteId");
        }
    }
}