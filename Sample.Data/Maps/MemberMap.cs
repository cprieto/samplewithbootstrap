﻿using FluentNHibernate.Mapping;

namespace Sample.Data.Maps
{
    public class MemberMap : ClassMap<Member>
    {
        public MemberMap()
        {
            Id(x => x.Email)
                .GeneratedBy
                .Assigned();
            Map(x => x.Name)
                .Column("Name")
                .Not.Nullable();
            Map(x => x.Password)
                .Column("Password");
            Map(x => x.CreatedAt)
                .Column("CreatedAt")
                .ReadOnly();
            References(x => x.Site)
                .Column("SiteId")
                .Cascade.None();
        }
    }
}