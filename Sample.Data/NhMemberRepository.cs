﻿using NHibernate;
using NLog;
using Sample.Core;

namespace Sample.Data
{
    public class NhMemberRepository : IMemberRepository
    {
        private readonly ISession session;
        private static Logger Log = LogManager.GetCurrentClassLogger();

        public NhMemberRepository(ISession session)
        {
            this.session = session;
        }

        public Member GetMemberByUsername(string username)
        {
            var member = session.Get<Member>(username);
            if (member == null)
                Log.Warn("Username '{0}' not found in database", username);

            return member;
        }
    }
}