﻿using NHibernate;
using NLog;

namespace Sample.Data
{
    public class NhTemplateRepository : ITemplateRepository
    {
        private readonly ISession session;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public NhTemplateRepository(ISession session)
        {
            this.session = session;
        }

        public Template GetTemplateForSite(int siteId)
        {
            var template = session
                .QueryOver<Template>()
                .Where(t => t.Site.Id == siteId)
                .SingleOrDefault();
            
            if (template == null)
                Log.Warn("Template for site with id {0} was not found!");

            return template;
        }
    }
}