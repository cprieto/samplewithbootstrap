﻿using System.Collections.Generic;
using Sample.Core;

namespace Sample.Default
{
    public class DefaultPlaceholder : IPlaceholderRenderer
    {
        private readonly IContext context;

        public DefaultPlaceholder(IContext context)
        {
            this.context = context;
        }

        public void Populate(IDictionary<string, string> dataBag)
        {
            dataBag["$name$"] = context.Member.Name;
        }
    }
}