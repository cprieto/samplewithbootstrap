﻿using System.Web.Mvc;
using BootstrapMvcSample.Controllers;
using NLog;

namespace Sample.App.Controllers
{
    public class AuthenticationController : BootstrapBaseController
    {
        private readonly IAuthenticationService authentication;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public AuthenticationController(IAuthenticationService authentication)
        {
            this.authentication = authentication;
        }

        public ViewResult Index(string returnUrl)
        {
            if (TempData.ContainsKey("username"))
                ViewBag.username = TempData["username"];

            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string username, string password, string returnUrl)
        {
            if (authentication.IsAuthenticated(username, password))
            {
                if (string.IsNullOrWhiteSpace(returnUrl))
                    return RedirectToAction("Index", "Home");
                return Redirect(returnUrl);
            }

            Error("Invalid username/password");

            TempData["username"] = username;
            return RedirectToAction("Index");
        }

        [Authorize]
        public RedirectToRouteResult Logout()
        {
            Log.Info("User '{0}' is logging out", User.Identity.Name);
            authentication.Logout();
            Information("You logged out");
            return RedirectToAction("Index", "Authentication");
        }
    }
}
