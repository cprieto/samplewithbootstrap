﻿using System.Web.Mvc;
using Sample.Services;

namespace Sample.App.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly MessageRenderer message;

        public HomeController(MessageRenderer message)
        {
            this.message = message;
        }

        public ActionResult Index()
        {
            ViewBag.Message = message.GetTheMessage();
            return View();
        }
    }
}
