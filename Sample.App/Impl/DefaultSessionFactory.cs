﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using Sample.Data.Maps;

namespace Sample.App.Impl
{
    public class DefaultSessionFactory
    {
        public static NHibernate.Cfg.Configuration GetConfiguration()
        {
            var cfg = Fluently.Configure()
                              .Database(
                                  MsSqlConfiguration.MsSql2008.ConnectionString(
                                      c => c.FromConnectionStringWithKey("default")))
                              .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MemberMap>());
            return cfg.BuildConfiguration();
        }
    }
}