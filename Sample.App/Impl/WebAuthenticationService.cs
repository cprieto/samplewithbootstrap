﻿using System;
using System.Web.Security;
using NLog;
using Sample.Core;

namespace Sample.App.Impl
{
    public class WebAuthenticationService : IAuthenticationService
    {
        private readonly IMemberRepository members;
        private static Logger Log = LogManager.GetCurrentClassLogger();

        public WebAuthenticationService(IMemberRepository members)
        {
            this.members = members;
        }

        public bool IsAuthenticated(string username, string password)
        {
            var member = members.GetMemberByUsername(username);
            if (member == null)
                return false;

            if (IsValidPassword(member.Password, password) == false)
            {
                Log.Warn("Failed authentication for user '{0}'", username);
                return false;
            }

            FormsAuthentication.SetAuthCookie(username, true);
            Log.Debug("User '{0}' authenticated", username);

            return true;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }

        private bool IsValidPassword(string stored, string password)
        {
            var hashed = password.Md5Hash();
            return stored.Equals(hashed, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}