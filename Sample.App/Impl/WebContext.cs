﻿using System.Web;
using Sample.Core;
using Sample.Default;

namespace Sample.App.Impl
{
    public class WebContext : IContext
    {
        private readonly Member member;
        private readonly Site site;

        public WebContext(IMemberRepository members, HttpContextBase httpContext)
        {
            if (httpContext.User.Identity.IsAuthenticated == false)
                return;

            member = members.GetMemberByUsername(httpContext.User.Identity.Name);
            if (member != null)
                site = member.Site;
        }

        public Site Site
        {
            get { return site; }
        }

        public Member Member
        {
            get { return member; }
        }
    }
}