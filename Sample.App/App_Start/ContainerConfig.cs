﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Sample.App.Modules;

namespace Sample.App
{
    public static class ContainerConfig
    {
        public static void RegisterContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<AggregateModule>();
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}