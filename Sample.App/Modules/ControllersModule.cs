﻿using Autofac;
using Autofac.Integration.Mvc;

namespace Sample.App.Modules
{
    public class ControllersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(ThisAssembly);
        }
    }
}