﻿using Autofac;
using Autofac.Integration.Mvc;
using Sample.App.Impl;
using Sample.Services;

namespace Sample.App.Modules
{
    public class AggregateModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<ControllersModule>();
            builder.RegisterModule<WebModule>();
            builder.RegisterModule<DataModule>();

            builder.RegisterType<MessageRenderer>()
                   .InstancePerHttpRequest();

            builder.RegisterType<DefaultTemplateRenderer>()
                   .As<ITemplateRenderer>()
                   .InstancePerHttpRequest();
        }
    }
}