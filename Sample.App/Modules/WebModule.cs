﻿using Autofac;
using Autofac.Integration.Mvc;
using Sample.App.Impl;
using Sample.Default;

namespace Sample.App.Modules
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WebAuthenticationService>()
                   .As<IAuthenticationService>()
                   .InstancePerHttpRequest();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterType<WebContext>()
                .As<IContext>()
                .InstancePerHttpRequest();
        }
    }
}