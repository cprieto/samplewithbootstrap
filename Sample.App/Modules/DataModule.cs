﻿using Autofac;
using Autofac.Integration.Mvc;
using NHibernate;
using NHibernate.Cfg;
using Sample.App.Impl;
using Sample.Core;
using Sample.Data;

namespace Sample.App.Modules
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x => DefaultSessionFactory.GetConfiguration())
                   .As<Configuration>()
                   .SingleInstance();

            builder.Register(x => x.Resolve<Configuration>().BuildSessionFactory())
                   .As<ISessionFactory>()
                   .SingleInstance();
            builder.Register(x => x.Resolve<ISessionFactory>().OpenSession())
                   .As<ISession>()
                   .OnRelease(x => x.Close())
                   .InstancePerHttpRequest();

            builder.RegisterType<NhMemberRepository>()
                   .As<IMemberRepository>()
                   .InstancePerHttpRequest();
            builder.RegisterType<NhTemplateRepository>()
                   .As<ITemplateRepository>()
                   .InstancePerHttpRequest();
        }
    }
}