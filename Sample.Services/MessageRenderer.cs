﻿using System.Collections.Generic;
using System.Linq;

namespace Sample.Services
{
    public class Lazy<T1, T2> //Autofac contains a proper implementation of this
    {
        public T1 Value { get; set; }
        public T2 Metadata { get; set; }
    }

    public class MessageRenderer
    {
        private readonly IContext context;
        private readonly ITemplateRepository templates;
        private readonly ITemplateRenderer renderer;

        public IEnumerable<Lazy<IPlaceholderRenderer, PlaceholderRendererMetadata>> PlaceholderRenderers { get; set; }

        public MessageRenderer(IContext context, ITemplateRepository templates, ITemplateRenderer renderer)
        {
            this.context = context;
            this.templates = templates;
            this.renderer = renderer;
        }

        public string GetTheMessage()
        {
            if (context.Member == null)
                return string.Empty;

            var template = templates.GetTemplateForSite(context.Site.Id);

            var specificRenderers = PlaceholderRenderers.Where(r => r.Metadata.SiteId == context.Site.Id).ToList();
            var fallbackRenderers =
                PlaceholderRenderers.Where(
                    r =>
                    r.Metadata.SiteId == null &&
                    specificRenderers.Any(s => s.Metadata.Name == r.Metadata.Name) == false);

            var dataBag = new Dictionary<string, string>();
            foreach (var placeholderRenderer in specificRenderers.Concat(fallbackRenderers).Select(r => r.Value))
            {
                placeholderRenderer.Populate(dataBag);
            }

            return renderer.Render(template.Content, dataBag);
        }
    }
}