﻿namespace Sample.Core
{
    public interface IMemberRepository
    {
        Member GetMemberByUsername(string username);
    }
}