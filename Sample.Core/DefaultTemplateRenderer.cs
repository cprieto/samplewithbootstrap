﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sample
{
    public class DefaultTemplateRenderer : ITemplateRenderer
    {
        public const string PlaceholderSyntax = @"\$[^\$]*\$";
        private static Regex regex = new Regex(PlaceholderSyntax, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public string Render(string template, IDictionary<string, string> context)
        {
            return context.Aggregate(template, (current, item) => current.Replace(item.Key, item.Value));
        }

        public IEnumerable<string> GetPlaceholders(string template)
        {
            var matches = regex.Matches(template);
            var items = from Match match in matches select match.Value;
            return items.Distinct();
        }
    }
}