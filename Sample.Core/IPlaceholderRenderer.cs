﻿using System.Collections.Generic;

namespace Sample
{
    public interface IPlaceholderRenderer
    {
        void Populate(IDictionary<string, string> dataBag);
    }
}