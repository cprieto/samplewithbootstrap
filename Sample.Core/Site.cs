﻿namespace Sample
{
    public class Site
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; protected set; }
    }
}