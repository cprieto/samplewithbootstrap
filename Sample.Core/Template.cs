﻿namespace Sample
{
    public class Template
    {
        public virtual int Id { get; protected set; }
        public virtual Site Site { get; protected set; }
        public virtual string Name { get; protected set; }
        public virtual string Content { get; protected set; }
    }
}