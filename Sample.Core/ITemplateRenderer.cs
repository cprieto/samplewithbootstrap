﻿using System.Collections.Generic;

namespace Sample
{
    public interface ITemplateRenderer
    {
        string Render(string template, IDictionary<string, string> context);
        IEnumerable<string> GetPlaceholders(string template);
    }
}