﻿namespace Sample
{
    public interface IContext
    {
        Site Site { get; }
        Member Member { get; }
    }
}