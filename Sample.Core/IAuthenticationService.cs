﻿namespace Sample
{
    public interface IAuthenticationService
    {
        bool IsAuthenticated(string username, string password);
        void Logout();
    }
}