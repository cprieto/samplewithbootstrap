﻿namespace Sample
{
    public interface ITemplateRepository
    {
        Template GetTemplateForSite(int siteId);
    }
}