﻿using System.Collections.Generic;
using NLog;

namespace Sample
{
    public class DefaultPlaceholderRenderer : IPlaceholderRenderer
    {
        private readonly IContext context;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public DefaultPlaceholderRenderer(IContext context)
        {
            this.context = context;
        }

        public void Populate(IDictionary<string, string> dataBag)
        {
            Log.Debug("Rendering name and money");

            dataBag["$name$"] = context.Member.Name;
            dataBag["$money$"] = "0";
        }
    }
}