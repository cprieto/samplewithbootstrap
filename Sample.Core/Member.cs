﻿using System;

namespace Sample
{
    public class Member
    {
        public virtual string Password { get; set; }
        public virtual string Name { get; protected set; }
        public virtual string Email { get; protected set; }
        public virtual DateTime CreatedAt { get; protected set; }
        public virtual Site Site { get; set; }
    }
}