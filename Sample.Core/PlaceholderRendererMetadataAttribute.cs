﻿using System;

namespace Sample
{
    public class PlaceholderRendererMetadataAttribute : Attribute
    {
        public string Name { get; private set; }
        public int? SiteId { get; private set; }

        public PlaceholderRendererMetadataAttribute(string name)
        {
            Name = name;
        }

        public PlaceholderRendererMetadataAttribute(string name, int siteId)
            : this(name)
        {
            SiteId = siteId;
        }
    }

    public class PlaceholderRendererMetadata
    {
        public string Name { get; set; }
        public int? SiteId { get; set; }
    }
}